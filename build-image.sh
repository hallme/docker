#!/bin/sh

cd "$CI_PROJECT_DIR/$CI_JOB_NAME"

mkdir -p "$CI_PROJECT_DIR/$CI_JOB_NAME/files/etc"
# Make sure the pacman.conf file is in the files path for each Dockerfile.
cp "$CI_PROJECT_DIR/pacman.conf" "$CI_PROJECT_DIR/$CI_JOB_NAME/files/etc/pacman.conf"

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker build --pull -t $CI_REGISTRY/$CI_PROJECT_PATH/$CI_JOB_NAME:latest .
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker push $CI_REGISTRY/$CI_PROJECT_PATH/$CI_JOB_NAME:latest
