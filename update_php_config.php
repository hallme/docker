<?php

$current_path = __DIR__;

$php_versions = [
    'php84' => '8.4.1-1',
    'php83' => '8.3.14-1',
    'php82' => '8.2.26-1',
    'php81' => '8.1.31-1',
    'php80' => '8.0.30-4',
    'php74' => '7.4.33-9',
    'php73' => '7.3.33-16',
    'php72' => '7.2.34-20',
    'php71' => '7.1.33-15',
    'php70' => '7.0.33-14',
];

foreach ($php_versions as $php => $version) {
    `rm -rf /tmp/phpdownload`;
    `mkdir /tmp/phpdownload`;
    file_put_contents('/tmp/phpdownload/php.tar.zst', file_get_contents("https://mirror.hallme.com/arch-php/$php-$version-x86_64.pkg.tar.zst"));
    `cd /tmp/phpdownload; tar -I 'zstd' -xvf php.tar.zst`;
    `cp /tmp/phpdownload/etc/$php/php.ini $current_path/php/files/etc/$php/php.stock.ini`;
    `rm -rf /tmp/phpdownload`;
}
